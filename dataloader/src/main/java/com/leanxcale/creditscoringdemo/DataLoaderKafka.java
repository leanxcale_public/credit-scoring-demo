package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.avro.LogicalTypes;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class DataLoaderKafka extends DataLoaderAbstract{
  private static String KAFKA_IP = Configuration.getProperty(CSD_PROPERTY.LOADER_KAFKA_IP, "localhost");
  private static String KAFKA_PORT = Configuration.getProperty(CSD_PROPERTY.LOADER_KAFKA_PORT, "9092");
  private static String KAFKA_SCHEMAREGISTRY_PORT = Configuration.getProperty(CSD_PROPERTY.LOADER_KAFKA_PORT_SCHEMAREGISTRY, "8081");

  private KafkaProducer producer;
  private Schema keySchema;
  private Schema valueSchema;

  public DataLoaderKafka(int threadId, LinkedBlockingQueue<String[]> queue) {
    super(threadId, queue);

    try {
      String kafkaUrl = KAFKA_IP + ":" + KAFKA_PORT;
      String schemaRegistryUrl = "http://" + KAFKA_IP + ":"+ KAFKA_SCHEMAREGISTRY_PORT;
      System.out.println("Connecting to Kafka:"+kafkaUrl);

      Properties props = new Properties(); // Create producer configuration
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl); // Set Kafka server ip:port
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, io.confluent.kafka.serializers.KafkaAvroSerializer.class); // Set record key serializer to Avro
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, io.confluent.kafka.serializers.KafkaAvroSerializer.class); // Set record value serializer to Avro
      props.put("schema.registry.url", schemaRegistryUrl);
      producer = new KafkaProducer(props); // Create producer

      // Create schema objects. We need an chema for the record key and another schema for the record value
      Schema tsType = LogicalTypes.timestampMillis().addToSchema(Schema.create(Schema.Type.LONG));
      // Define the key schema as only one field of Integer type called "id"
      keySchema = SchemaBuilder.record("myKey").namespace("com.leanxcale").fields().name("id").type().longType().noDefault().endRecord();

      valueSchema = getSchema(); // Define the value schema with the rest of the fields defined as columns in the csv file

    }catch (Exception e){
      System.err.println("Error connecting to kafka ip:"+KAFKA_IP);
      e.printStackTrace();
    }
  }

  @Override
  protected void insert(Map<String, Object> recordMap) {
    // Generate record key
    GenericRecord avroKeyRecord = new GenericData.Record(keySchema); // Create new record following key schema
    avroKeyRecord.put("id", recordMap.get("id")); // Put autoincrement value

    GenericRecord avroValueRecord = getValueRecord(recordMap);

    // Create a new producer record for topic: "loans", key: created key crecord, value: created value record
    ProducerRecord<Object, Object> record = new ProducerRecord<>("loans", avroKeyRecord, avroValueRecord);
    producer.send(record); // Send the record to Kafka server

  }

  @Override
  protected void close() {
    if (producer != null){
      producer.flush();
      producer.close();
    }
  }


  private GenericRecord getValueRecord(Map<String, Object> values){
    int random = rand.nextInt(11); //modify values +-10% random
    GenericRecord avroValueRecord = new GenericData.Record(valueSchema); // Create new record following value schema

    avroValueRecord.put("loan_status", values.get("loan_status"));
    avroValueRecord.put("loan_amnt", values.get("loan_amnt"));
    avroValueRecord.put("term",values.get("term"));
    avroValueRecord.put("int_rate", values.get("int_rate"));
    avroValueRecord.put("installment", values.get("installment"));
    avroValueRecord.put("grade", values.get("grade"));
    avroValueRecord.put("emp_length", values.get("emp_length"));
    avroValueRecord.put("home_ownership", values.get("home_ownership"));
    avroValueRecord.put("annual_inc", values.get("annual_inc"));
    avroValueRecord.put("verification_status", values.get("verification_status"));
    avroValueRecord.put("issue_d", values.get("issue_d"));
    avroValueRecord.put("mths_since_issue_d",values.get("mths_since_issue_d"));
    avroValueRecord.put("desc", values.get("desc"));
    avroValueRecord.put("purpose",values.get("purpose"));
    avroValueRecord.put("title", values.get("title"));
    avroValueRecord.put("zip_code",values.get("zip_code"));
    avroValueRecord.put("addr_state", values.get("addr_state"));
    avroValueRecord.put("dti", values.get("dti"));
    avroValueRecord.put("mths_since_earliest_cr_line", values.get("mths_since_earliest_cr_line"));
    avroValueRecord.put("inq_last_6mths", values.get("inq_last_6mths"));
    avroValueRecord.put("revol_util", values.get("revol_util"));
    avroValueRecord.put("out_prncp", values.get("out_prncp"));
    avroValueRecord.put("total_pymnt", values.get("total_pymnt"));
    avroValueRecord.put("total_rec_int", values.get("total_rec_int"));
    avroValueRecord.put("last_pymnt_d",values.get("last_pymnt_d"));
    avroValueRecord.put("mths_since_last_pymnt_d", values.get("mths_since_last_pymnt_d"));
    avroValueRecord.put("mths_since_last_credit_pull_d",values.get("mths_since_last_credit_pull_d"));
    avroValueRecord.put("total_rev_hi_lim", values.get("total_rev_hi_lim"));

    return avroValueRecord;
  }

  protected Schema getSchema() {
    Schema valueSchema = SchemaBuilder.record("myValue").namespace("com.leanxcale").fields()
        .name("loan_status").type().intType().noDefault()
        .name("loan_amnt").type().doubleType().noDefault()
        .name("term").type().intType().noDefault()
        .name("int_rate").type().doubleType().noDefault()
        .name("installment").type().doubleType().noDefault()
        .name("grade").type().stringType().noDefault()
        .name("emp_length").type().doubleType().noDefault()
        .name("home_ownership").type().stringType().noDefault()
        .name("annual_inc").type().doubleType().noDefault()
        .name("verification_status").type().stringType().noDefault()
        .name("issue_d").type().longType().noDefault()
        .name("mths_since_issue_d").type().intType().noDefault()
        .name("desc").type().stringType().noDefault()
        .name("purpose").type().stringType().noDefault()
        .name("title").type().stringType().noDefault()
        .name("zip_code").type().stringType().noDefault()
        .name("addr_state").type().stringType().noDefault()
        .name("dti").type().doubleType().noDefault()
        .name("mths_since_earliest_cr_line").type().intType().noDefault()
        .name("inq_last_6mths").type().doubleType().noDefault()
        .name("revol_util").type().doubleType().noDefault()
        .name("out_prncp").type().doubleType().noDefault()
        .name("total_pymnt").type().doubleType().noDefault()
        .name("total_rec_int").type().doubleType().noDefault()
        .name("last_pymnt_d").type().stringType().noDefault()
        .name("mths_since_last_pymnt_d").type().intType().noDefault()
        .name("mths_since_last_credit_pull_d").type().intType().noDefault()
        .name("total_rev_hi_lim").type().doubleType().noDefault()
        .endRecord();
    return valueSchema;
  }

}
