package com.leanxcale.creditscoringdemo;

import static com.leanxcale.creditscoringdemo.DataLoaderAbstract.LAST_SECOND_TS;
import static com.leanxcale.creditscoringdemo.DataLoaderAbstract.MAX_LOAD_SECOND;
import static com.leanxcale.creditscoringdemo.DataLoaderAbstract.PRODUCED_COUNT;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import java.util.Arrays;
import java.util.HashSet;

/**
 * This class reads the given csv and insert its data into LX. This loader can run in 2 modes, depending on the value of
 * the property "loader.mode":
 *  - [direct]:inserts data directly in LX table
 *  - [kafka]:send data to a kafka topic
 *
 * The total amount of loans ingested will be defined by:
 *     Total lines of input csv * "loader.kafka.threads" * "loader.csv.repetitions"
 */
public class DataLoaderRunner {

  private enum LOADER_MODE {
    DIRECT, KAFKA;

    public static LOADER_MODE fromString(String m) {
      for (LOADER_MODE mode : LOADER_MODE.values()) {
        if (mode.toString().equalsIgnoreCase(m)) {
          System.out.println("LOADER running on mode:" + mode);
          return mode;
        }
      }
      throw new IllegalArgumentException("Mode [" + m + "] is not a valid loader mode. Valid ones are:" + Arrays.toString(LOADER_MODE.values()));
    }
  }

  private static final LOADER_MODE MODE = LOADER_MODE.fromString(Configuration.getProperty(CSD_PROPERTY.LOADER_MODE));
  public static final String ENABLE_HACK_PARAM = "-eh"; //FIXME JP remove
  public static final String LOANS_FILE_DEFAULT = "loans.csv";

  private static final int N_THREADS = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.LOADER_THREADS, "7"));
      //each thread insert a full dataset copy. more threads = more data ingested per seconf and more total amount of data

  private static final String WINDOW_SIZE_DEFAULT = "9";

  public static void main(String[] args) throws Exception {
    String lxURL = UrlBuilder.getLxUrl();

    int windowSize = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.LOADER_STATS_WINDOW_SIZE, WINDOW_SIZE_DEFAULT));
    boolean insertRateHackEnabled = new HashSet<String>(Arrays.asList(args)).contains(ENABLE_HACK_PARAM);//FIXME remove
    UpdateInsertionRate updateInsertionRate = new UpdateInsertionRate(lxURL, windowSize, insertRateHackEnabled);
    updateInsertionRate.start();

    int nThreads = N_THREADS;
    switch (MODE){
      case DIRECT:
        System.out.println("Overwriting n_threads("+ CSD_PROPERTY.LOADER_THREADS.getKey() +") values to 1 due direct mode ");
        nThreads = 1; //just one thread in direct mode. performance is not the target here
        break;
    }

    String loansFile = Configuration.getProperty(CSD_PROPERTY.LOADER_LOANS_FILE, LOANS_FILE_DEFAULT);
    CSVReader reader = new CSVReader(nThreads, loansFile);
    reader.start();

    for (int i = 0; i < nThreads; i++) {
      switch (MODE) {
        case KAFKA:
          DataLoaderKafka kafkaLoader = new DataLoaderKafka(i, reader.queueMap.get(i));
          kafkaLoader.start();
          break;
        case DIRECT:
          DataLoaderDirect directLoader = new DataLoaderDirect(i, reader.queueMap.get(i));
          directLoader.start();
      }
    }


    //Controls the max messages per second send to kafka in order to avoid collapse Kafka. If a high load come into kafka
    //the consumers read rate slows down
    new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println("Starting Load Controller.Max load / sec:" + MAX_LOAD_SECOND);
        while (true) {
          try {
            System.out.println("Produced: " + PRODUCED_COUNT.getAndSet(0) + " / sec");
            LAST_SECOND_TS.set(System.currentTimeMillis());
            Thread.sleep(998);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }).start();
  }
}
