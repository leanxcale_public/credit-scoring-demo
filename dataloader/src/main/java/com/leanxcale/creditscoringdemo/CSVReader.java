package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class CSVReader extends Thread{

  private static final int REPETITIONS = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.LOADER_CSV_REPETITIONS, "2"));; //times that each row will be inserted

  public Map<Integer,LinkedBlockingQueue<String[]>> queueMap = new HashMap<>();
  private String inputFile;
  private static final int SIZE = 500;

  public CSVReader(int nThreads, String inputFile){
    this.inputFile = inputFile;
    for (int i = 0; i < nThreads; i++){
      queueMap.put(i, new LinkedBlockingQueue(SIZE));
    }
  }

  @Override
  public void run() {
    // Open and read csv
    try( BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(this.inputFile)))){
      System.out.println("Reading data file:" + this.inputFile);
      reader.readLine();

      Collection<LinkedBlockingQueue<String[]>> queues = queueMap.values();
      String line;
      while((line = reader.readLine())!=null){
        String[] values = line.split(",");
        for (LinkedBlockingQueue queue : queues){
          for(int i = 0; i < REPETITIONS; i++) {
            queue.put(values);
          }
        }
      }
      System.out.println("data file fully loaded.");

    }
    // Flush the producer to ensure it has all been written to Kafka and finally close it
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
