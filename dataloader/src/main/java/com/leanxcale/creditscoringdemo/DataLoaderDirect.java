package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;

public class DataLoaderDirect  extends DataLoaderAbstract{

  private String lxURL = UrlBuilder.getLxUrl();
  private Settings settings = Settings.parse(lxURL);
  private String tableName = Configuration.getProperty(CSD_PROPERTY.LOADER_DIRECT_TABLE, "loans");
  private Session session;
  private Table table;
  private int batchCounter = 0;
  private int BATCH_SIZE = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.LOADER_DIRECT_BATCH_SIZE, "1000"));

  public DataLoaderDirect(int threadId, LinkedBlockingQueue<String[]> queue) throws LeanxcaleException, IOException {
    super(threadId, queue);
    session = SessionFactory.newSession(settings);
    table = session.database().getTable(tableName);
    if (table == null) {
      throw new RuntimeException("Table ["+tableName+"] does not exists");
    }
  }


  @Override
  protected void insert(Map<String, Object> record) {
    Tuple tuple = table.createTuple();
    for (Entry<String, Object> entry: record.entrySet()){
      tuple.put(entry.getKey(), entry.getValue());
    }
    table.insert(tuple);
    if (++batchCounter >= BATCH_SIZE){
      session.commit();
      batchCounter = 0;
    }

  }

  @Override
  protected void close() {
    session.commit();
    session.close();
  }


}
