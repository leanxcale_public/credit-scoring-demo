package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public abstract class DataLoaderAbstract extends Thread {

  protected final DateFormat formatter = new SimpleDateFormat("MMM-yyyy", Locale.US);
  protected final Date NOW_DATE = new Date();
  protected final Calendar CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

  private static final AtomicLong GLOBAL_ID_INC = new AtomicLong(0);
      // Id will be the PK of the destination table and it's an autoincrement field

  static final int MAX_LOAD_SECOND =
      Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.LOADER_MAX_LOAD_SEC, "300000"));
  static final AtomicInteger PRODUCED_COUNT = new AtomicInteger(0);
  static final AtomicLong LAST_SECOND_TS = new AtomicLong(System.currentTimeMillis());

  protected int threadId;
  protected LinkedBlockingQueue<String[]> queue;
  protected Random rand = new Random();

  public DataLoaderAbstract(int threadId, LinkedBlockingQueue<String[]> queue) {
    this.queue = queue;
    this.threadId = threadId;
    System.out.println("Created Thread with id:" + this.threadId);
  }

  protected abstract void insert(Map<String, Object> record);
  protected abstract void close();

  @Override
  public void run() {

    try {
      String values[] = null;
      while (true) {
        try {
          if (!((values = queue.take()) != null)) {
            break;
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        try {
          Map<String, Object> recordMap = getLoanRecord(values);
          insert(recordMap);

          int cont = PRODUCED_COUNT.incrementAndGet();
          if (cont >= MAX_LOAD_SECOND) {
            long sleep = 1000 - (System.currentTimeMillis() - LAST_SECOND_TS.get());
            //              System.out.println("Limit["+MAX_LOAD_SECOND+" ] reached. sleeping:"+sleep);
            Thread.sleep(sleep);
          }

        } catch (Exception e) {
          System.err.println("[" + threadId + "] Error parsing line -> " + Arrays.toString(values));
          e.printStackTrace();
          continue;
        }
      }
    }finally {
      close();
    }


  }

  protected Map<String, Object> getLoanRecord(String[] values) throws ParseException {
    HashMap<String, Object> map = new HashMap<>();

    long id = GLOBAL_ID_INC.addAndGet(1); //creates thread-safe unique autoincremental id
    map.put("id", id);


    int random = rand.nextInt(11); //modify values +-10% random
    boolean add = (random % 2 == 0);

    String loanStatus = values[11];

    int loan_status = 0; // Rejected
    if ( ! "rejected".equals(loanStatus)) {
      // Accepted
      if (loanStatus.contains("Fully Paid")) {
        loan_status = 1;
      } else if (loanStatus.contains("Charged Off")) {
        loan_status = 2;
      } else {
        loan_status = 3;
      }
    }
    map.put("loan_status", loan_status);

    map.put("loan_amnt", readDouble(values[1], random, add));
    map.put("term", readInt(values[2]));
    map.put("int_rate", readDouble(values[3], random, add));
    map.put("installment", readDouble(values[4], random, add));
    map.put("grade", values[5]);
    map.put("emp_length", readDouble(values[6], random, add));
    map.put("home_ownership", values[7]);
    map.put("annual_inc", readDouble(values[8], random, add));
    map.put("verification_status", values[9]);
    int monthsSince_issue_d = getMonthsSince(values[10]);
    long ts = getRandomTs(values[10]); //get a random day of that month
    map.put("issue_d", ts);
    map.put("mths_since_issue_d", monthsSince_issue_d);
    map.put("desc", values[12]);
    map.put("purpose", values[13]);
    map.put("title", values[14]);
    map.put("zip_code", values[15]);
    map.put("addr_state", values[16]);
    map.put("dti", readDouble(values[17], random, add));
    int mths_since_earliest_cr_line = getMonthsSince(values[18]);
    map.put("mths_since_earliest_cr_line", mths_since_earliest_cr_line);
    map.put("inq_last_6mths", readDouble(values[19], random, add));
    map.put("revol_util", readDouble(values[20], random, add));
    map.put("out_prncp", readDouble(values[21], random, add));
    map.put("total_pymnt", readDouble(values[22], random, add));
    map.put("total_rec_int", readDouble(values[23], random, add));
    int mths_since_last_pymnt_d = getMonthsSince(values[24]);
    map.put("last_pymnt_d", values[24]);
    map.put("mths_since_last_pymnt_d", mths_since_last_pymnt_d);
    int mths_since_last_credit_pull_d = getMonthsSince(values[25]);
    map.put("mths_since_last_credit_pull_d", mths_since_last_credit_pull_d);
    double total_rev_hi_lim = (values.length == 27) ? readDouble(values[26], random, add)
        : 0; //if last field is empty CSVReaded split(,) does not include it on the array
    map.put("total_rev_hi_lim", total_rev_hi_lim);

    return map;
  }


  /**
   * @param d: date in format: May-2007
   * @return a ts of a random day of the given month
   */
  protected Map<String, Long> monthTsCache = new HashMap<>(120);

  protected long getRandomTs(String d) throws ParseException {
    Long monthTs = monthTsCache.get(d);
    if (monthTs == null) {
      Date date = formatter.parse(d);
      CALENDAR.setTime(date);
      monthTs = CALENDAR.getTimeInMillis();
      monthTsCache.put(d, monthTs);
    }

    return monthTs;
  }


  protected Map<String, Integer> monthsSinceCache = new HashMap<>(120);

  protected int getMonthsSince(String value) throws ParseException {
    if (value == null || "".equals(value)) {
      return 0;
    }
    Integer months = monthsSinceCache.get(value);
    if (months == null) {
      CALENDAR.setTime(formatter.parse(value));
      int nMonthFrom = 12 * CALENDAR.get(Calendar.YEAR) + CALENDAR.get(Calendar.MONTH);
      CALENDAR.setTime(NOW_DATE);
      int nMonthNow = 12 * CALENDAR.get(Calendar.YEAR) + CALENDAR.get(Calendar.MONTH);
      months = nMonthNow - nMonthFrom;
      monthsSinceCache.put(value, months);
    }
    return months;
  }


  protected int readInt(String value) {
    return "".equals(value) ? 0 : Integer.parseInt(value);
  }

  protected double readDouble(String value, int random, boolean add) {
    if ("".equals(value)) {
      return 0;
    } else {
      double v = Double.parseDouble(value);
      double delta = random * v / 100;
      if (add) {
        return v + delta;
      } else {
        return v - delta;
      }
    }
  }

}
