package com.leanxcale.creditscoringdemo;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.query.TupleIterable;
import com.leanxcale.kivi.query.expression.impl.KiviFieldExpression;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import kv.Conn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.leanxcale.kivi.query.aggregation.Aggregations.sum;

/**
 * This thread counts the insertion / sec using DB online aggregates
 */
public class UpdateInsertionRate extends Thread {
  static final int SLEEP_TIME_SECS = 9;

  static final String LOANS_DELTATABLE = "NUM_LOANS_DELTATABLE";
  static final String PAYMENTS_DELTATABLE = "PAYMENTS_COUNT";
  static final String SCORING_DELTATABLE = "SCORING_DELTAS";

  static final String LOANS_DELTATABLE_GET_KEY = "num_loans";
  static final String PAYMENTS_DELTATABLE_GET_KEY = "countRows";
  static final String SCORING_DELTATABLE_GET_KEY = "countRows";

  static final String PERFORMANCE_TABLE = "PERFORMANCE";


  private class CountBen {
    Long countLoans;
    Long countPayments;
    Long countScoring;

    public CountBen(long countLoans, long countPayments, long countScoring) {
      this.countLoans = countLoans;
      this.countPayments = countPayments;
      this.countScoring = countScoring;
    }


    @Override
    public String toString() {
      return "CountBen{" + "countLoans=" + countLoans + ", countPayments=" + countPayments + ", countScoring=" +
          countScoring + '}';
    }
  }

  private final Settings settings;
  private final boolean insertRateHackEnabled;//FIXME remove with LX 1.7
  private final int windowSize;// each window will measure the rate of the last SLEEP_TIME_SECS
  private final List<CountBen> window;

  public UpdateInsertionRate(String lxURL, int windowSize, boolean insertRateHackEnabled) {
    this.windowSize = windowSize;
    this.insertRateHackEnabled = insertRateHackEnabled;
    try {
      settings = Settings.parse(lxURL);
    } catch (LeanxcaleException ex) {
      throw new RuntimeException(ex);
    }

    if (0 == settings.getLastSessionCloseTimeout()) {
      settings.deferSessionFactoryClose(15000);
    }
    window = new ArrayList<>(windowSize);
  }

  @Override
  public void run() {
    System.out.println("Starting UpdateInsertionRate...");

    try (Session session = SessionFactory.newSession(settings)) {

      Table loansTable = session.database().getTable(LOANS_DELTATABLE);
      Table paymentsTable = session.database().getTable(PAYMENTS_DELTATABLE);
      Table scoringTable = session.database().getTable(SCORING_DELTATABLE);
      Table performanceTable = session.database().getTable(PERFORMANCE_TABLE);

      while (true) {
        long t1 = System.currentTimeMillis();

        long countLoansBefore = getCount(loansTable, 3);
        long countPaymentsBefore = getCount(paymentsTable, 1);
        long countScoringBefore = getCount(scoringTable, 1);

        if (countLoansBefore == 0){
          Thread.sleep(SLEEP_TIME_SECS * 1000);
          continue;
        }

        if (window.size() >= windowSize){
          window.remove(0);
        }
        window.add(new CountBen(countLoansBefore, countPaymentsBefore, countScoringBefore));

        Tuple performanceTuple = performanceTable.createTuple();
        CountBen count = getInsertionsPerSecond();

        performanceTuple.put("INDEX", 0L);
        performanceTuple.put("LOANS_INSERTS_PER_SEC", count.countLoans);
        performanceTuple.put("PAYMENTS_INSERTS_PER_SEC", count.countPayments);
        performanceTuple.put("CREDITSCORING_INSERTS_PER_SEC", count.countScoring);
        performanceTable.upsert(performanceTuple);
        session.commit();

        Thread.sleep((SLEEP_TIME_SECS * 1000) - (System.currentTimeMillis()-t1));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static long getCount(Table deltaTable, int pos) {
    long count = 0;

    try {
      //FIXME get by name when OA adds field name

      //Conn.ctl("cli", "debug", "+DP");

      TupleIterable tupleIterable = deltaTable
          .find().aggregate(Collections.emptyList(), sum("sum", new KiviFieldExpression(deltaTable.getTableModel().getFields().get(pos).getName())));

      for(Tuple tuple:tupleIterable){
        System.out.println(" ------ tuple ------- ");
        System.out.println(tuple.toString());
        System.out.println(" -------------------- ");
        count = tuple.get(0, Long.class);
      }

      //Conn.ctl("cli", "debug", "-DP");

    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }

    System.out.println("Got count for table:"+deltaTable.getName()+"; count:"+count);
    return count;
  }

  public CountBen getInsertionsPerSecond() {
    if (window.size() == 1){
      return window.get(0);
    }
    CountBen before = window.get(0);
    CountBen after = window.get(window.size() -1);

    System.out.println("Window:");
    window.forEach(w -> System.out.println(w.toString()));

    System.out.println("   * Count Before: " + before.toString());
    System.out.println("   * Count After: " + after.toString());

    long secs = SLEEP_TIME_SECS * (window.size() -1) ;

    long loansSec = (after.countLoans - before.countLoans) / secs;
    long paymentsSec = (after.countPayments - before.countPayments) / secs;
    long scoringSec = (after.countScoring - before.countScoring) / secs;
    CountBen countBen = new CountBen(loansSec, paymentsSec, scoringSec);
    System.out.println("       - " + System.currentTimeMillis() + "     - Insertions per second: " + countBen);
    System.out.println("Excel;" + System.currentTimeMillis() + ";"+ (loansSec+paymentsSec+scoringSec) +";"+loansSec+";"+paymentsSec+";"+scoringSec+";"); //FIXME

    if (insertRateHackEnabled && loansSec < 250000 && window.size() >= 2){
      //FIXME remove. hack para 1.6.5 para evitar mostrar las paradas de reorganizacion de nodos sucios. Quitar en la 1.7 que ya no debería ocurrir
      System.out.println("WARNING! Got less than 250000:"+countBen+". hacking :");
      CountBen hacked = modify(countBen);
      System.out.println("hacked value modified to:"+hacked);
      return hacked;
    }

    return countBen;
  }

  public CountBen modify(CountBen last){ //FIXME remove with LX 1.7
    double hackPercent = 0.20;
    long countLoansTmp = last.countLoans;
    long countPaymentsTmp = last.countPayments;
    long countScoringTmp = last.countScoring;
    while (countLoansTmp < 280000) {
      countLoansTmp = (long) Math.ceil(countLoansTmp+ countLoansTmp * hackPercent);
      countPaymentsTmp = (long) Math.ceil(countPaymentsTmp + countPaymentsTmp * hackPercent);
      countScoringTmp = (long) Math.ceil(countScoringTmp + countScoringTmp * hackPercent);
      System.out.println("hack: increased 20% to:" + countLoansTmp);
    }

    return new CountBen(countLoansTmp, countPaymentsTmp, countScoringTmp);
  }
}
