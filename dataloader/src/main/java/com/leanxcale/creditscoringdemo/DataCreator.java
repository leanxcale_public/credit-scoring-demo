package com.leanxcale.creditscoringdemo;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class DataCreator {
  public static Set<String> REQUIRED_COLUMNS = new HashSet<>(
      Arrays.asList("id", "loan_amnt", "grade", "home_ownership", "verification_status", "purpose",
          "term", "int_rate", "annual_inc", "dti", "inq_last_6mths", "revol_util", "out_prncp", "total_pymnt", "total_rec_int",
          "total_rev_hi_lim", "earliest_cr_line", "since_issue_d", "last_credit_pull_d", "zip_code", "addr_state",
          "installment", "issue_d", "last_pymnt_d", "total_pymnt", "emp_length", "desc", "title"));


  private static final DateFormat formmaterAccepted = new SimpleDateFormat("MMM-yyyy", Locale.US);
  private static final DateFormat formmaterRejected = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
  private static final Calendar CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

  //not uploaded to git due to they are vey big. just MERGE_OUT zipped is uploaded to git
  //can be download from here: https://www.kaggle.com/wordsforthewise/lending-club
  private static String ACCEPTED = "assets/data/accepted_2007_to_2018Q4.csv";
  private static String ACCEPTED_OUT = "assets/data/accepted_2007_to_2018Q4_ordered.csv";
  private static String REJECTED = "assets/data/rejected_2007_to_2018Q4.csv";
  private static String REJECTED_OUT = "assets/data/rejected_2007_to_2018Q4_ordered.csv";
  private static String MERGE_OUT = "assets/merged.csv";
  private static String CLEAN_OUT = "assets/loans.csv";

  public static void main(String[] args) throws IOException, ParseException {
//    sortCsv(ACCEPTED, ACCEPTED_OUT, "issue_d", formmaterAccepted);  //Sort require tons of memory. At least, give 20-24Gb to the jvm heap
//    sortCsv(REJECTED, REJECTED_OUT, "Application Date", formmaterRejected);

//    merge(ACCEPTED_OUT, REJECTED_OUT, MERGE_OUT); //merge accepted and rejected in date order

    cleanDates(MERGE_OUT, CLEAN_OUT);
  }

  private static void cleanDates(String in, String out) throws IOException, ParseException {
    InputStream inA = new FileInputStream(new File(in));
    Writer writer = new FileWriter(out);
    CSVWriter csvWriter = new CSVWriter(writer);

    try (CSVReader csvReaderAccepted = new CSVReader(new BufferedReader(new InputStreamReader(inA)))) {
      String[] line;
      while( (line = csvReaderAccepted.readNext()) != null) {
        List<String> outLines = new ArrayList<>();
        for (String entry : line) {
          String o = entry.replaceAll("months", "").trim();
          o = o.replaceAll("10\\+ years", "10").replaceAll("< 1 year", "0").replaceAll("years", "")
              .replaceAll("year", "").trim();
          outLines.add(o);
        }
        String[] outArray = new String[line.length];
        outArray = outLines.toArray(outArray);
        csvWriter.writeNext(outArray, false);
      }

    } finally {
      csvWriter.close();
      writer.close();
    }
  }


  private static void merge(String acceptedOut, String rejectedOut, String mergeOut) throws IOException, ParseException {
    InputStream inA = new FileInputStream(new File(acceptedOut));
    Writer writer = new FileWriter(mergeOut);
    CSVWriter csvWriter = new CSVWriter(writer);

    try (CSVReader csvReaderAccepted = new CSVReader(new BufferedReader(new InputStreamReader(inA)))){
      String[] header = csvReaderAccepted.readNext();
      Set<Integer> requiredColumnsPos = getRequiredColumnsPos(header);
      String[] headerFiltered = filterLine(header, requiredColumnsPos);
      csvWriter.writeNext(headerFiltered, false);
      int acceptedDateCol = getColumnPos(headerFiltered, "issue_d");
      int numberOfColumns = headerFiltered.length;

      InputStream inR = new FileInputStream(new File(rejectedOut));
      try (CSVReader csvReaderRejected = new CSVReader(new BufferedReader(new InputStreamReader(inR)))){
        String[] headerRej = csvReaderRejected.readNext();
        int rejectedDateCol = getColumnPos(headerRej, "Application Date");

        String[] vAccepted = csvReaderAccepted.readNext();
        String[] vRejected = csvReaderRejected.readNext();
        while ( vAccepted != null && vRejected != null){
          String[] acceptedFiltered = filterLine(vAccepted, requiredColumnsPos);
          CALENDAR.setTime(formmaterAccepted.parse(acceptedFiltered[acceptedDateCol]));
          long acceptedMillis = CALENDAR.getTimeInMillis();
          CALENDAR.setTime(formmaterRejected.parse(vRejected[rejectedDateCol]));
          long rejectedMillis = CALENDAR.getTimeInMillis();

          if (acceptedMillis <= rejectedMillis){
            csvWriter.writeNext(acceptedFiltered, false);
            vAccepted = csvReaderAccepted.readNext();
          }else{
            String[] values = buildRejectedArray(vRejected, numberOfColumns);
            csvWriter.writeNext(values, false);
            vRejected = csvReaderRejected.readNext();
          }
        }
        
        //copy the pending elements
        if (vAccepted != null) {
          while (vAccepted != null) {
            csvWriter.writeNext(filterLine(vAccepted, requiredColumnsPos), false);
            vAccepted = csvReaderAccepted.readNext();
          }
        }
        if (vRejected != null) {
          while (vRejected != null) {
            String[] values = buildRejectedArray(vRejected, numberOfColumns);
            csvWriter.writeNext(values, false);
            vRejected = csvReaderRejected.readNext();
          }
        }
      }

    }finally {
      csvWriter.close();
      writer.close();
    }
  }

  private static String[] filterLine(String[] line, Set<Integer> requiredColumnsPos) {
    List<String> list = new ArrayList<>();
    for (int i = 0; i < line.length; i++){
      if (requiredColumnsPos.contains(i))
        list.add(line[i].replaceAll(",", "")); //remove , from all fields to avoid problems parsing csv
    }
    list.add("-");// ensure the last column has a value in order to ensure the loader split(,) always return the same number of fields
    String[] array = new String[list.size()];
    return list.toArray(array);
  }

  private static Set<Integer> getRequiredColumnsPos(String[] header) {
    Set<Integer> set = new HashSet<>();
    for (int i = 0; i < header.length; i++){
      if (REQUIRED_COLUMNS.contains(header[i].toLowerCase())){
        set.add(i);
      }
    }
    return set;
  }

  /* convert to accepted format */
  private static String[] buildRejectedArray(String[] vRejected, int lenght) throws ParseException {
    String amount = vRejected[0];
    Date date = formmaterRejected.parse(vRejected[1]);
    String dateFormat = formmaterAccepted.format(date);
    String title = vRejected[2].replaceAll(",", "");
    String debIncRatio = vRejected[4].replaceAll("%","").trim();
    String zipcode = vRejected[5];
    String state = vRejected[6];
    String empLength = vRejected[7];
    String[] array = new String[lenght];
    array[1] = amount;
    array[2] = "36 months"; //term
    array[3] = "21.0"; //int_rate
    array[5] = "G"; //grade
    array[6] = empLength;
    array[7] = "OWN"; //home_ownership
    array[8] = String.valueOf(ThreadLocalRandom.current().nextInt(30000, 160000)); //annual_inc
    array[9] = "Verified"; //verification_status
    array[10] = dateFormat; //issue_d
    array[11] = "rejected";
    int randomNum = ThreadLocalRandom.current().nextInt(0, PURPOSES.size());
    array[13] = PURPOSES.get(randomNum); //purpose
    array[14] = title;
    array[15] = zipcode;
    array[16] = state;
    array[17] = debIncRatio; //dti  //36 is the worst
    array[18] = ""; //earliest_cr_line
    array[19] = "0.0"; //inq_last_6mths
    array[20] = "1.1"; //revol_util
    array[21] = "1285.0"; //out_prncp
    array[22] = "0"; //total_pymnt
    array[23] = "8000.0"; //total_rec_int
    array[24] = ""; //last_pymnt_d
    array[25] = ""; //last_credit_pull_d
    array[26] = "80000.0"; //total_rev_hi_lim
    array[27] = "-"; //control header
    return array;
  }

  private static void sortCsv(String input, String output, String dateColumn, DateFormat formmater) throws IOException {
    InputStream in = new FileInputStream(new File(input));
    try (CSVReader csvReader = new CSVReader(new BufferedReader(new InputStreamReader(in)))) {
      System.out.println("Reading accepted file:" + input);
      String[] header = csvReader.readNext();
      int pos = getColumnPos(header,dateColumn);
      String[] values;
      ArrayList<String[] > list = new ArrayList<>();
      int i = 0;
      while ((values = csvReader.readNext()) != null) {
        try {
          CALENDAR.setTime(formmater.parse(values[pos]));
          list.add(values);
        } catch (ParseException e) {
          System.err.println("invalid date:"+values[pos] + " at line:"+i + "; id:"+values[0]);
        }

        i++;
        if ((i % 100000) == 0){
          System.out.println("Reading line:"+i);
        }
      }
      System.out.println("File loaded");

      Collections.sort(list, new Comparator<String[]>() { //order by date
        @Override
        public int compare(String[] o1, String[] o2) {
          try {
            CALENDAR.setTime(formmater.parse(o1[pos]));
            Date o1Date = CALENDAR.getTime();
            CALENDAR.setTime(formmater.parse(o2[pos]));
            Date o2Date = CALENDAR.getTime();
            return o1Date.compareTo(o2Date);
          } catch (ParseException e) {
            return -1;
          }
        }
      });

      System.out.println("list sorted. Writing to:"+output);
      //write
      Writer writer = new FileWriter(output);
      CSVWriter csvWriter = new CSVWriter(writer);
      csvWriter.writeNext(header, false);
      csvWriter.writeAll(list, false);
      csvWriter.close();
      writer.close();
    }
  }

  private static int getColumnPos(String[] header, String name) {
    for (int i = 0; i < header.length; i++){
      if (header[i].equalsIgnoreCase(name)){
        return i;
      }
    }
    throw new RuntimeException("Column " + name + " not found");
  }

  private static final List<String> PURPOSES = new ArrayList<String>() {{
    add("vacation");
    add("house");
    add("wedding");
    add("medical");
    add("other");
    add("major_purchase");
    add("car");
    add("home_improvement");
    //we want to make more probable to be selected randomly the next purposes as they are the worst ranked
    add("educational");add("educational");add("educational");add("educational");add("educational");
    add("renewable_energy");add("renewable_energy");add("renewable_energy");add("renewable_energy");add("renewable_energy");
    add("small_business");add("small_business");add("small_business");add("small_business");add("small_business");
    add("moving");add("moving");add("moving");add("moving");add("moving");
    add("debt_consolidation");add("debt_consolidation");add("debt_consolidation");add("debt_consolidation");
    add("credit_card");add("credit_card");add("credit_card");
  }};
}
