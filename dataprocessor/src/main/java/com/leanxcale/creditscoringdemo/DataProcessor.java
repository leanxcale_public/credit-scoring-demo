/*
package com.leanxcale.creditscoringdemo;

import com.leanxcale.CommandLineMain;
import com.leanxcale.spark.LeanxcaleDataSource;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.spark.sql.*;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;


//FIXME NOT PRODUCTIVE! need to add streaming read (new at 1.7)

*/
/**
 * Uses spark to process data.
 *//*
public class DataProcessor extends CommandLineMain implements Serializable{
  private static String LX_URL;

  private static final String TABLE_INPUT = "LOANS";
  private static final String TABLE_OUTPUT_SCORING = "SCORING";
  private static final String TABLE_OUTPUT_PAYMENTS = "PAYMENTS";

  private static final double LOAN_APPROVAL_CUT_OFF = 425;

  private final Calendar paymentsCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final Calendar monthsFromCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final Calendar monthsToCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final DateFormat formater = new SimpleDateFormat("MMM-yyyy", Locale.US);

  private static final int READ_BATCH_SIZE = 100000;

  public static void main(String[] args) throws Exception {
    new DataProcessor().execute(args);
  }

  private SparkSession spark;

  private final LinkedBlockingQueue<Row> INPUT_QUEUE = new LinkedBlockingQueue<>(500000);
  private final LinkedBlockingQueue<Scoring> SCORING_QUEUE = new LinkedBlockingQueue<>(500000);
  private final LinkedBlockingQueue<Payment> PAYMENTS_QUEUE = new LinkedBlockingQueue<>(500000);

  private Map<String, Integer> columnPositions = null;



  @Override
  protected void addOptions(Options options) {
    CommandLineUtils.addUrlOptions(options);
  }

  @Override
  public void execute(CommandLine commandLine) throws Exception {
    LX_URL = CommandLineUtils.getLxUrl(commandLine);
    System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX] - Starting Processor");
    spark = SparkSession.builder().appName("CreditScoringDemo").getOrCreate();

    new Reader().start();
    new Processor().start();
    new ScoringWritter().start();
    new PaymentsWritter().start();
  }

  private class Reader extends Thread implements Serializable{
    long lastId = 0;

    @Override
    public void run() {
      while (true) {
        try{
          int lastInputCount = 0;
          long t1 = System.currentTimeMillis();
          Dataset<Row> inputData = readData(spark, READ_BATCH_SIZE);
          if (columnPositions == null){
            columnPositions = getColumnsPos(inputData.columns());
          }
          Iterator<Row> it = inputData.toLocalIterator();
          long tmpId = lastId;
          while (it.hasNext()){
            Row row = it.next();
            long id = row.getLong(columnPositions.get("id"));
            if (tmpId +1 != id ){
              System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX]" +
                ". Missed id:" + (tmpId +1) + " breaking iteration");
              break;
            }
            tmpId = id;
            INPUT_QUEUE.put(row);
            lastInputCount++;
          }
          lastId = tmpId;

          if (lastInputCount < READ_BATCH_SIZE){
            System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+
                "[LX] - Received incomplete batch from DB.count:"+lastInputCount +" Waiting...");
            Thread.sleep(1000);
          }

          long t2 = System.currentTimeMillis();
          System.out.println("[" + System.currentTimeMillis() +"] " + Thread.currentThread().getId()   + "[LX] " +
              "Read cycle done. Read["+ lastInputCount + "] in ["+(t2-t1) + "]. queue size:"+INPUT_QUEUE.size());

        }catch (Exception e){
          System.err.println("Error reading data");
          e.printStackTrace();
        }
      }
    }


    private Dataset<Row> readData(SparkSession spark, int size) {
      return spark.read()  //read data in blocks
          .format(LeanxcaleDataSource.SHORT_NAME).option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL)
          .option(LeanxcaleDataSource.TABLE_PARAM, TABLE_INPUT)
          .load().filter(getFilter(size))
          .cache();
    }

    private String getFilter(int size) {
      long from = (lastId+1);
      long to = from + size;
      String filter = "id >= " + from + " and id < " + to;
      System.out.println("[" + System.currentTimeMillis() +"] " + Thread.currentThread().getId()  + "[LX] - Loading with filter:"+filter);
      return filter;
    }
  }

  private class ScoringWritter extends Writter<Scoring>{

    @Override
    protected Scoring take() throws InterruptedException {
      return SCORING_QUEUE.take();
    }

    @Override
    protected Encoder getEncoder() {
      return Encoders.bean(Scoring.class);
    }

    @Override
    protected String getTableName() {
      return TABLE_OUTPUT_SCORING;
    }
  }

  private class PaymentsWritter extends Writter<Payment>{

    @Override
    protected Payment take() throws InterruptedException {
      return PAYMENTS_QUEUE.take();
    }

    @Override
    protected Encoder getEncoder() {
      return Encoders.bean(Payment.class);
    }

    @Override
    protected String getTableName() {
      return TABLE_OUTPUT_PAYMENTS;
    }
  }

  private abstract class Writter<T> extends Thread implements Serializable{
    protected abstract T take() throws InterruptedException;
    protected abstract Encoder getEncoder();
    protected abstract String getTableName();

    private static final int WRITE_BATCH = 100000;

    private Writter(){
      //force first call to avoid table overwritting. See method. com.leanxcale.spark.write.DataSetWriter.write for more details
      spark.createDataset(new ArrayList<>(), getEncoder()).write().format(LeanxcaleDataSource.SHORT_NAME).mode(SaveMode.Append)
          .option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL).saveAsTable(getTableName());
    }

    @Override
    public void run() {
      long t1 = System.currentTimeMillis();
      while (true){
        try{
          List<T> tmpList = new ArrayList<>(WRITE_BATCH);
          while (tmpList.size() < WRITE_BATCH){
            tmpList.add(take());
          }
          Dataset dataset = spark.createDataset(tmpList, getEncoder());
          write(dataset);
          long t2 = System.currentTimeMillis();
          System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX] - " +
              "Written["+WRITE_BATCH+"] at table:"+getTableName()+" in ["+(t2 -t1) + "] ms");
          t1 = t2;

        }catch (Exception e){
          System.err.println("Error writting data");
          e.printStackTrace();
        }
      }
    }

    private void write(Dataset<?> outputData) {
      //TODO add multithread, pool, commit,... new properties
      outputData.write().format(LeanxcaleDataSource.SHORT_NAME).mode(SaveMode.Append)
          .option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL).saveAsTable(getTableName());//TODO transactional ?
    }
  }

  private class Processor extends Thread implements Serializable{
    @Override
    public void run() {
      long t1 = System.currentTimeMillis();
      int cont = 0;
      while (true){
        try{
          Row row = INPUT_QUEUE.take();
          Scoring scoring = processScoring(row);
          SCORING_QUEUE.put(scoring);
          if ( row.getInt(columnPositions.get("loan_accepted")) == 1){
            List<Payment> payments = processPayments(row);
            payments.forEach(p-> {
              try {
                PAYMENTS_QUEUE.put(p);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            });
          }
          cont++;
          if (cont >= READ_BATCH_SIZE){
            long t2 = System.currentTimeMillis();
            System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX] - " +
                "Processed["+cont+"] in ["+(t2 -t1) + "] ms. Queues:"+SCORING_QUEUE.size() + " / "+PAYMENTS_QUEUE.size());
            cont = 0;
            t1 = t2;
          }

        }catch (Exception e){
          System.err.println("Error processing data");
          e.printStackTrace();
        }
      }
    }

    private Scoring processScoring(Row row) {
      double score = 598;
      long id = row.getLong(columnPositions.get("id"));

      score += getGradeScore(row, columnPositions, id);
      score += getHomeOwnershipScore(row, columnPositions);
      score += getVerificationStatusScore(row, columnPositions, id);
      score += getPurposeScore(row, columnPositions, id);
      score += getTermScore(row, columnPositions, id);
      score += getIntRateScore(row, columnPositions);
      score += getAnualIncScore(row, columnPositions);
      score += getDtiScore(row, columnPositions);
      score += getInqLast6Score(row, columnPositions);
      score += getRevolUtiScore(row, columnPositions);
      score += getOutPrncpScore(row, columnPositions);
      score += getTotalPymntScore(row, columnPositions);
      score += getTotalRecIntScore(row, columnPositions);
      score += getTotalRevHiLimScore(row, columnPositions);
      score += getMonthsSinceEarliestCrLineScore(row, columnPositions);
      score += getMonthsSinceIssueDScore(row, columnPositions);
      score += getMonthsSinceLastCreditPullD(row, columnPositions);

      boolean approved = (score > LOAN_APPROVAL_CUT_OFF);
      //          System.out.println("[" + threadId + "] Got score[" + score + "], approval:[" + approved + "] for id:" + id);

      return new Scoring(id, score, approved);
    }

    private List<Payment> processPayments(Row row) {
      long id = row.getLong(columnPositions.get("id"));
      int loanPaid = row.getInt(columnPositions.get("loan_paid"));
//      int loan_accepted = row.getInt(columnPositions.get("loan_accepted"));
      double installment = row.getDouble(columnPositions.get("installment"));
      long issue_d = row.getLong(columnPositions.get("issue_d"));
      int term = row.getInt(columnPositions.get("term"));

      List<Payment> payments = new ArrayList<>();
      paymentsCalendar.setTimeInMillis(issue_d);
//      if (loan_accepted == 1){
        if (loanPaid == 1){ //credit paid successfully
          for (int i = 1; i <= term; i++){
            payments.add(new Payment(id, i, paymentsCalendar.getTimeInMillis(), installment, installment));
            paymentsCalendar.add(Calendar.MONTH, 1);
          }

        }else { //accepted and not paid
          String last_pymnt_d = row.getString(columnPositions.get("last_pymnt_d"));
          double total_pymnt = row.getDouble(columnPositions.get("total_pymnt"));
          int monthsPaid = getMonthsBetween(issue_d, last_pymnt_d);
          double monthlyPaid = total_pymnt / monthsPaid;
          for (int i = 1; i <= term; i++) {
            double paid = (i <= monthsPaid) ? monthlyPaid : 0;
            payments.add(new Payment(id, i, paymentsCalendar.getTimeInMillis(), installment, paid));
            paymentsCalendar.add(Calendar.MONTH, 1);
          }
        }
//      }//else rejected. does not generated payments

      return payments;
    }

    private  double getMonthsSinceLastCreditPullD(Row row, Map<String, Integer> columnPositions) {
      int value = row.getInt(columnPositions.get("mths_since_last_credit_pull_d"));
      if (value == 0)
        return 15;
      if (value < 56)
        return 16;
      if (value < 61)
        return 30;
      if (value < 75)
        return 7;
      return 0;
    }

    private  double getMonthsSinceIssueDScore(Row row, Map<String, Integer> columnPositions) {
      int value = row.getInt(columnPositions.get("mths_since_issue_d"));
      if (value < 79)
        return -9;
      if (value < 89)
        return -8;
      if (value < 100)
        return -8;
      if (value < 122)
        return -3;
      return 0;
    }

    private  double getMonthsSinceEarliestCrLineScore(Row row, Map<String, Integer> columnPositions) {
      int value = row.getInt(columnPositions.get("mths_since_earliest_cr_line"));
      if (value == 0)
        return 3;
      if (value < 125)
        return -4;
      if ( value >= 167 && value < 249)
        return 0;
      if (value >= 331 && value < 434)
        return 1;
      return 0;
    }

    private  double getTotalRevHiLimScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("total_rev_hi_lim"));
      if (value == 0)
        return 5;
      if (value < 6381)
        return 9;
      if ( value >= 6381 && value < 19144)
        return 8;
      if (value >= 19144 && value < 25525)
        return 5;
      if (value >= 25525 && value < 35097)
        return 2;
      if (value >= 35097 && value < 54241)
        return 1;
      if (value >= 54241 && value < 79780)
        return 0;
      return 0;
    }

    private  double getTotalRecIntScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("total_rec_int"));
      if (value < 1089)
        return 109;
      if ( value >= 1089 && value < 2541)
        return 94;
      if (value >= 2541 && value < 4719)
        return 66;
      if (value >= 4719 && value < 7260)
        return 34;
      return 0;
    }

    private  double getTotalPymntScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("total_pymnt"));
      if (value < 10000)
        return -155;
      if ( value >= 10000 && value < 15000)
        return -101;
      if (value >= 15000 && value < 20000)
        return -73;
      if (value >= 20000 && value < 25000)
        return -47;
      return 0;
    }

    private  double getOutPrncpScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("out_prncp"));
      if (value < 1286)
        return -88;
      if ( value >= 1286 && value < 6432)
        return -27;
      if (value >= 6432 && value < 9005)
        return -16;
      if (value >= 9005 && value < 10291)
        return -10;
      if (value >= 10291 && value < 15437)
        return -7;
      return 0;
    }

    private  double getRevolUtiScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("revol_util"));
      if (value == 0) //missing
        return -2;
      if (value < 0.1)
        return -4;
      if ( value >= 0.1 && value < 0.2)
        return -14;
      if (value >= 0.2 && value < 0.3)
        return 11;
      if (value >= 0.3 && value < 0.4)
        return -3;
      if (value >= 0.4 && value < 0.5)
        return 8;
      if (value >= 0.5 && value < 0.6)
        return 10;
      if ( value >= 0.6 && value < 0.7)
        return -6;
      if (value >= 0.7 && value < 0.8)
        return 1;
      if (value >= 0.8 && value < 0.9)
        return -1;
      if (value >= 0.9 && value < 1.0)
        return 15;
      return 0; //> 1
    }

    private  double getInqLast6Score(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("inq_last_6mths"));
      if (value == 0) //missing
        return 3; //we cannot distinguish between 0 (10) and missing(3)
      if ( value == 1 || value == 2)
        return 9;
      if ( value == 3 || value == 4)
        return 6;
      return 0; //> 4
    }

    private  double getDtiScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("dti"));
      if (value < 1.6)
        return 13;
      if ( value >= 1.6 && value < 5.599)
        return 12;
      if (value >= 5.599 && value < 10.397)
        return 8;
      if (value >= 10.397 && value < 15.196)
        return 6;
      if (value >= 15.196 && value < 19.195)
        return 3;
      if (value >= 19.195 && value < 24.794)
        return 1;
      if (value >= 24.794 && value < 35.191)
        return 1;
      return 0; //>35.191
    }

    private  double getAnualIncScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("annual_inc"));
      if (value == 0) //missing
        return 15;
      if (value < 28555)
        return 12;
      if ( value >= 28555 && value < 37440)
        return 11;
      if (value >= 37440 && value < 61137)
        return 10;
      if (value >= 61137 && value < 81872)
        return 9;
      if (value >= 81872 && value < 102606)
        return 8;
      if (value >= 102606 && value < 120379)
        return 5;
      if (value >= 120379 && value < 150000)
        return 4;
      return 0; //> 150k
    }

    private  double getIntRateScore(Row row, Map<String, Integer> columnPositions) {
      double value = row.getDouble(columnPositions.get("int_rate"));
      if (value < 7.071)
        return 24;
      if ( value >= 7.071 && value < 10.374)
        return 7;
      if (value >= 10.374 && value <13.676)
        return 1;
      if (value >= 13.676 && value <15.74)
        return 1;
      if (value >= 15.74 && value < 20.281)
        return 0;
      return 0; //>= 20.281
    }

    private  double getTermScore(Row row, Map<String, Integer> columnPositions, long id) {
      int value = row.getInt(columnPositions.get("term"));
      switch (value) {
        case 36:
          return -2;
        case 60:
          return 0;
        default:
          System.err.println("Invalid term value:["+ value+ "] at id:"+id);
          return 0;
      }
    }

    private  double getPurposeScore(Row row, Map<String, Integer> columnPositions, long id) {
      String value = row.getString(columnPositions.get("purpose"));
      switch (value) {
        case "debt_consolidation":
          return -8;
        case "credit_card":
          return -6;

        case "vacation":
        case "house":
        case "wedding":
        case "medical":
        case "other":
          return 0;

        case "major_purchase":
        case "car":
        case "home_improvement":
          return 0;

        case "educational":
        case "renewable_energy":
        case "small_business":
        case "moving":
          return -12;

        default:
          System.err.println("Invalid purpose value:["+ value+ "] at id:"+id);
          return 0;
      }
    }

    private  double getVerificationStatusScore(Row row, Map<String, Integer> columnPositions, long id) {
      String value = row.getString(columnPositions.get("verification_status"));
      switch (value) {
        case "Source Verified":
          return -7;
        case "Verified":
          return -11;
        case "Not Verified":
          return 0;
        default:
          System.err.println("Invalid verification_status value:["+ value+ "] at id:"+id);
          return 0;
      }
    }

    private  double getHomeOwnershipScore(Row row, Map<String, Integer> columnPositions) {
      String value = row.getString(columnPositions.get("home_ownership"));
      switch (value) {
        case "OWN":
          return -1;
        case "MORTGAGE":
          return 0;
        default:
          return -3;
      }
    }

    private  double getGradeScore(Row row, Map<String, Integer> columnPositions, long id) {
      String grade = row.getString(columnPositions.get("grade"));
      switch (grade){
        case "A":
          return 24;
        case "B":
          return 20;
        case "C":
          return 15;
        case "D":
          return 12;
        case "E":
          return 8;
        case "F":
          return 5;
        case "G":
          return 0;
        default:
          System.err.println("Invalid grade value:["+ grade + "] at id:"+id);
          return 0;
      }
    }

    private int getMonthsBetween(long issue_d, String last_pymnt_d) {
      try {
        if(last_pymnt_d == null || last_pymnt_d.equals("")){
          return 0;
        }
        monthsFromCalendar.setTimeInMillis(issue_d);
        monthsToCalendar.setTime(formater.parse(last_pymnt_d));

        int nMonthFrom = 12 * monthsFromCalendar.get(Calendar.YEAR) + monthsFromCalendar.get(Calendar.MONTH);
        int nMonthTo = 12 * monthsToCalendar.get(Calendar.YEAR) + monthsToCalendar.get(Calendar.MONTH);
        return nMonthTo - nMonthFrom;
      }catch (ParseException e){
        System.err.println("Cannot parse date:"+last_pymnt_d);
        e.printStackTrace();
        return 0;
      }
    }
  }


  public static Set<String> REQUIRED_COLUMNS = new HashSet<>(Arrays.asList("id", "grade", "home_ownership", "verification_status", "purpose",
      "term", "int_rate", "annual_inc", "dti", "inq_last_6mths", "revol_util", "out_prncp", "total_pymnt", "total_rec_int",
      "total_rev_hi_lim", "mths_since_earliest_cr_line", "mths_since_issue_d", "mths_since_last_credit_pull_d",
      "installment", "issue_d", "loan_accepted", "loan_paid", "last_pymnt_d", "total_pymnt"));
  public static Map<String, Integer> getColumnsPos(String[] columns) {
    Map<String, Integer> map = new HashMap<>();
    for (int i = 0; i < columns.length; i++){
      if (REQUIRED_COLUMNS.contains(columns[i].toLowerCase())){
        map.put(columns[i].toLowerCase(), i);
      }
    }
    return map;
  }

}
*/
