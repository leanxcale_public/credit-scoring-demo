#!/bin/bash

LXVERSION=$(./lx-version.sh)

echo "Uploading Superset assets to AWS - Testing..."

bucket=lx-demos

fileName=dashboard.json
file=./superset_assets/${fileName}
remoteFileLocation=${DEMO_FOLDER}/superset_assets/${LXVERSION}/${fileName}

aws s3 cp $file s3://$bucket/$remoteFileLocation


echo "${fileName} uploaded"

fileName=datasources.yaml
file=./superset_assets/${fileName}
remoteFileLocation=${DEMO_FOLDER}/superset_assets/${LXVERSION}/${fileName}

aws s3 cp $file s3://$bucket/$remoteFileLocation

echo "${fileName} uploaded"