package com.leanxcale.creditscoringdemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

  public enum CSD_PROPERTY{
    //global properties
    LEANXCALE_IP("leanxcale.ip"),
    LEANXCALE_USER("leanxcale.user"),
    LEANXCALE_PASSWORD("leanxcale.password"),
    LEANXCALE_SECURE("leanxcale.secure"),
    LEANXCALE_CERTIFICATE("leanxcale.certificate"),
    LEANXCALE_TRANSACTIONAL("leanxcale.transactional"),
    LEANXCALE_PORT_KIVI("leanxcale.port.kivi"),
    LEANXCALE_PORT_JDBC("leanxcale.port.jdbc"),
    LEANXCALE_SCHEMA("leanxcale.schema"),

    //connection urls. not exposed at config file by default. It should only be used if you know what are you doing. For common users this urls will be automatically created from previous properties. If specified these values will overwrite previous ones
    LEANXCALE_CONNECTION_URL_KIVI("leanxcale.kivi.connectionurl"),
    LEANXCALE_CONNECTION_URL_JDBC("leanxcale.jdbc.url"),

    //loader properties
    LOADER_MODE("loader.mode"),
    LOADER_CSV_REPETITIONS("loader.csv.repetitions"),
    LOADER_THREADS("loader.kafka.threads"),
    LOADER_MAX_LOAD_SEC("loader.max.load.sec"),
    LOADER_KAFKA_IP("loader.kafka.ip"),
    LOADER_KAFKA_PORT("loader.kafka.port"),
    LOADER_KAFKA_PORT_SCHEMAREGISTRY("loader.kafka.schemaregistry.port"),
    LOADER_LOANS_FILE("loader.loans.file"),
    LOADER_DIRECT_TABLE("loader.direct.table"),
    LOADER_DIRECT_BATCH_SIZE("loader.direct.batch.size"),

    LOADER_STATS_WINDOW_SIZE("loader.stats.window.size"),

    //processor properties
    PROCESSOR_QUEUE_READER_SIZE("processor.queue.reader.size"),
    PROCESSOR_QUEUE_SCORING_SIZE("processor.queue.scoring.size"),
    PROCESSOR_QUEUE_PAYMENTS_SIZE("processor.queue.payments.size"),

    PROCESSOR_THREADS_PROCESSOR("processor.threads.processor"),
    PROCESSOR_THREADS_SCORING("processor.threads.scoring"),
    PROCESSOR_THREADS_PAYMENTS("processor.threads.payments"),

    PROCESSOR_STATS_FREQUENCY("processor.stats.frequency"),

    PROCESSOR_WRITER_SCORING_BATCH("processor.writer.scoring.batch.size"),
    PROCESSOR_WRITER_PAYMENT_BATCH("processor.writer.payment.batch.size");


    public String getKey() {
      return key;
    }

    private String key;

    CSD_PROPERTY(String key){
      this.key = key;
    }
  }

  private static Properties properties = new Properties();;

  private static final String CONFIG_FILE_PROPERTY = "CSD_CONFIG";

  static {
    String configFile = System.getProperty(CONFIG_FILE_PROPERTY);
    if (configFile == null){
      System.err.println("NOT FOUND JVM CONFIG ARG: ["+ CONFIG_FILE_PROPERTY +"]");
    }else{
      try {
        System.out.println("Loading config from file:"+configFile);
        properties.load(new FileInputStream(new File(configFile)));
        System.out.println("Properties loaded: "+ properties.toString());
      } catch (IOException e) {
        System.err.println("Error loading config file:"+configFile);
        e.printStackTrace();
      }
    }
  }

  public static String getProperty(CSD_PROPERTY property, String defValue){
    return properties.getProperty(property.key, defValue);
  }

  public static String getProperty(CSD_PROPERTY property){
    return properties.getProperty(property.key);
  }
}
