package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;

public class UrlBuilder {

  private static final String KIVI_URL = "lx://#IP#:#KIVI_PORT#/#SCHEMA#";
  private static final String KIVI_CERT_PROP_SUFFIX = ";CERT=";

  private static final String JDBC_URL = "jdbc:leanxcale://#IP#:#JDBC_PORT#/#SCHEMA#";
  private static final String JDBC_SECURE_SUFFIX = ";secure=true";


  private String lxUrl;
  private String jdbcUrl;

  private static UrlBuilder instance = null;

  private UrlBuilder(){
    String ip = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_IP, "localhost");
    buildLxUrl(ip);
    buildJdbcUrl(ip);
  }

  private static UrlBuilder getInstance() {
    if (instance == null){
      synchronized (UrlBuilder.class){
        if(instance == null){
          instance = new UrlBuilder();
        }
      }
    }
    return instance;
  }

  private void buildJdbcUrl(String ip) {
    if (Configuration.getProperty(CSD_PROPERTY.LEANXCALE_CONNECTION_URL_JDBC) != null) {
      this.jdbcUrl = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_CONNECTION_URL_JDBC);
      System.out.println("Got jdbc url directly from property[" + CSD_PROPERTY.LEANXCALE_CONNECTION_URL_JDBC.getKey() + "]:" + this.jdbcUrl);

    } else {
      String port = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_PORT_JDBC, "1522");
      String schema = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_SCHEMA, "db");
      String jdbcURL = JDBC_URL.replaceAll("#IP#", ip)
          .replaceAll("#JDBC_PORT#", port)
          .replaceAll("#SCHEMA#", schema);
      if (Boolean.parseBoolean(Configuration.getProperty(CSD_PROPERTY.LEANXCALE_SECURE, "false"))) {
        jdbcURL = jdbcURL + JDBC_SECURE_SUFFIX;
      }
      this.jdbcUrl = jdbcURL;
      System.out.println("Built JDBC url:"+ this.jdbcUrl);
    }
  }

  private void buildLxUrl(String ip) {
    if (Configuration.getProperty(CSD_PROPERTY.LEANXCALE_CONNECTION_URL_KIVI) != null){
      this.lxUrl = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_CONNECTION_URL_KIVI);
      System.out.println("Got kivi connection url directly from property["+CSD_PROPERTY.LEANXCALE_CONNECTION_URL_KIVI.getKey()+"]:"+this.lxUrl);

    }else {
      String port = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_PORT_KIVI, "9876");
      String schema = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_SCHEMA, "db");
      String lxURL = KIVI_URL.replaceAll("#IP#", ip)
          .replaceAll("#KIVI_PORT#", port)
          .replaceAll("#SCHEMA#", schema);

      if (Boolean.parseBoolean(Configuration.getProperty(CSD_PROPERTY.LEANXCALE_SECURE, "false"))) {
        String cert = Configuration.getProperty(CSD_PROPERTY.LEANXCALE_CERTIFICATE);
        lxURL += KIVI_CERT_PROP_SUFFIX + cert;
      }

      lxURL += ";USER=" + Configuration.getProperty(CSD_PROPERTY.LEANXCALE_USER, "APP");
      this.lxUrl = lxURL;
      System.out.println("Built kivi connection:"+this.lxUrl);
    }
  }

  public static String getLxUrl(){
    return getInstance().lxUrl;
  }

  public static String getJdbcUrl(){
    return getInstance().jdbcUrl;
  }

}
