#This script will be copied from amiUpdater when building AMIs
echo "Creating tables"
java -cp "lib/*" -DCSD_CONFIG=config.properties com.leanxcale.creditscoringdemo.CreateTablesWithAggregates

echo "Running Standalone processor"
nohup java -cp "lib/*" -Xms4G -Xmx4G -DCSD_CONFIG=config.properties com.leanxcale.creditscoringdemo.StandaloneProcessor > nohup_standalone.out 2>&1 &

echo "Running data loader"
#nohup java -classpath lib/*.jar -DCSD_CONFIG=config.properties com.leanxcale.creditscoringdemo.DataLoaderRunner > nohup_loader.out 2>&1 &
nohup java -cp "lib/*" -Xms4G -Xmx4G -DCSD_CONFIG=config.properties com.leanxcale.creditscoringdemo.DataLoaderRunner > nohup_loader.out 2>&1 &



