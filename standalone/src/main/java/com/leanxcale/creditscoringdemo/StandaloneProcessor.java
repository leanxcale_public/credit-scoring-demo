package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import com.leanxcale.kivi.tuple.Tuple;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 *  This process reads have 3 groups of threads:
 *    - Readers: Read info from Leanxcale and store it in a queue
 *    - Processor: Pulls the info from the previous queue, process it, and store the results in output queues
 *    - Writers: Read info from the output processor queues and store it in Leanxcale
 */
public class StandaloneProcessor{
  private static final int INPUT_QUEUE_SIZE = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_QUEUE_READER_SIZE, "300000"));
  private static final int SCORING_QUEUE_SIZE = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_QUEUE_SCORING_SIZE, "500000"));
  private static final int PAYMENTS_QUEUE_SIZE = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_QUEUE_PAYMENTS_SIZE, "5000000"));
  private static final int FREQUENCY_MS = Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_STATS_FREQUENCY, "10000"));;

  public static final AtomicLong READ_COUNT = new AtomicLong(0);
  public static final AtomicLong PROCESSOR_COUNT = new AtomicLong(0);
  public static final AtomicLong SCORING_COUNT = new AtomicLong(0);
  public static final AtomicLong PAYMENTS_COUNT = new AtomicLong(0);

  private LinkedBlockingQueue<Tuple> INPUT_QUEUE = new LinkedBlockingQueue<>(INPUT_QUEUE_SIZE);
  private LinkedBlockingQueue<Scoring> SCORING_QUEUE = new LinkedBlockingQueue<>(SCORING_QUEUE_SIZE);
  private LinkedBlockingQueue<Payment> PAYMENTS_QUEUE = new LinkedBlockingQueue<>(PAYMENTS_QUEUE_SIZE);


  public static void main(String[] args) throws Exception {
    new StandaloneProcessor().execute(args);
  }


  public void execute(String[] args) throws Exception {
    ConnectionProvider connectionProvider = new ConnectionProvider(UrlBuilder.getLxUrl());

    new Reader(INPUT_QUEUE, connectionProvider).start();

    for (int i = 0; i < Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_THREADS_PROCESSOR, "2")); i++){
      new Processor(INPUT_QUEUE, SCORING_QUEUE, PAYMENTS_QUEUE).start();
    }

    for (int i = 0; i < Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_THREADS_SCORING, "2")); i++) {
      new ScoringWriter(SCORING_QUEUE, connectionProvider).start();
    }
    for (int i = 0; i < Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_THREADS_PAYMENTS, "5")); i++) {
      new PaymentsWriter(PAYMENTS_QUEUE, connectionProvider).start();
    }

    stats();
  }

  private void stats(){
    while (true){
      try{
        long readCount = READ_COUNT.getAndSet(0);
        long processorCount = PROCESSOR_COUNT.getAndSet(0);
        long scoringCount = SCORING_COUNT.getAndSet(0);
        long paymentsCount = PAYMENTS_COUNT.getAndSet(0);
        int readSize = INPUT_QUEUE.size();
        int scSize = SCORING_QUEUE.size();
        int pymnSize = PAYMENTS_QUEUE.size();

        StringBuffer buf = new StringBuffer(" - ").append("\n");
        buf.append("READ: Processed:[").append(readCount).append("]; queue:[").append(readSize).append("/").append(INPUT_QUEUE_SIZE).append("] -> ").append(readSize*100/INPUT_QUEUE_SIZE).append("% \n");
        buf.append("SCORING: Processed:[").append(scoringCount).append("]; queue:[").append(scSize).append("/").append(SCORING_QUEUE_SIZE).append("] -> ").append(scSize*100/SCORING_QUEUE_SIZE).append("% \n");
        buf.append("PAYMENT: Processed:[").append(paymentsCount).append("]; queue:[").append(pymnSize).append("/").append(PAYMENTS_QUEUE_SIZE).append("] -> ").append(pymnSize*100/PAYMENTS_QUEUE_SIZE).append("% \n");
        buf.append("PROC: Processed:[").append(processorCount).append("]").append("\n");
        System.out.println(System.currentTimeMillis() + buf.toString());

        Thread.sleep(FREQUENCY_MS - 10);
      }catch (Exception e){
        e.printStackTrace();
      }
    }
  }
}
