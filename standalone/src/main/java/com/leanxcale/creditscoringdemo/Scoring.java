package com.leanxcale.creditscoringdemo;

public class Scoring {

  long id;
  double score;
  boolean approved;

  public Scoring(){}

  public Scoring(long id, double score, boolean approved) {
    this.id = id;
    this.score = score;
    this.approved = approved;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  @Override
  public String toString() {
    return "Scoring{" + "id=" + id + ", score=" + score + ", approved=" + approved + '}';
  }
}
