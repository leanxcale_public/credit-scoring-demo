package com.leanxcale.creditscoringdemo;

import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;
import com.leanxcale.kivi.tuple.TupleKey;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;


/** Read info from Leanxcale and store it in a queue */
public class Reader extends Thread{
  public static final int READ_BATCH_SIZE = 10000;
  private static final String TABLE_INPUT = "LOANS";

  private final ConnectionProvider connProvider;

  private LinkedBlockingQueue<Tuple> queue;

  private long lastId = 0;

  public Reader(LinkedBlockingQueue<Tuple> queue, ConnectionProvider connProvider) {
    this.queue = queue;
    this.connProvider = connProvider;
  }

  /*
  TODO multithreading: the problem is that now we are reading from LX in ascending order by ID. As this process works
  parallel with a Kafka that inserts data into LX we hace to simulate some kind of streaming. As kafka is always working in
  multithread mode, can happen two thinks that make that input read from LX is not fully "consistent":
    - a incomplete batch: due to we ask for 100k elements but less are stored in DB
    - some id is "missed" because kafka has not already inserted it. For example, the last id inserted by kafka´s thread X is
        500 but other threads already has not inserted, lets say, 497 and 495. So, the problem is thar we dont konw the real last
        is read until we processed all retrived that.

    these problems should be solved in order to be able to read parally from LX
   */

  @Override
  public void run() {
    while (true) {
      try{
        //        long t1 = System.currentTimeMillis();

        long count = readData();
        StandaloneProcessor.READ_COUNT.addAndGet(count);

        if (count < READ_BATCH_SIZE){
          System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+
              "[LX] - Received incomplete batch from DB.count:"+count +" Waiting...");
          Thread.sleep(1000);
        }

        //        long t2 = System.currentTimeMillis();
        //        System.out.println("[" + System.currentTimeMillis() +"] " + Thread.currentThread().getId()   + "[LX] " +
        //            "Read cycle done. Read["+ count + "] in ["+(t2-t1) + "]. queue size:"+queue.size());

      }catch (Exception e){
        System.err.println("Error reading data");
        e.printStackTrace();
      }
    }
  }

  private long readData() {
    long[] limits = getLimits();
    try(Session session = connProvider.getNewSession()) {
      //      System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX]" + //FIXME
      //          ". Reading from:" + limits[0] + "; to:"+limits[1]);
      Table table = session.database().getTable(TABLE_INPUT);
      TupleKey min = (TupleKey) table.createTupleKey().put("ID", limits[0]);
      TupleKey max = (TupleKey) table.createTupleKey().put("ID", limits[1]);
      Iterator<Tuple> it = table.find().min(min).max(max).sort().iterator();
      while (it.hasNext()){
        Tuple t = it.next();
        Long id = t.get("ID",Long.class);
        if (++lastId != id){
          System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX]" +
              ". Missed id:" + lastId + ".");
          lastId--;
          break;
        }else{
          queue.put(t);
        }
      }


    } catch (Exception e) {
      System.err.println("Error reading data:");
      e.printStackTrace();
    }
    return (lastId - limits[0] +1);
  }

  private long[] getLimits(){
    long from = (lastId+1);
    long to = from + READ_BATCH_SIZE;
    long[] array = new long[2];
    array[0] = from;
    array[1] = to;
    return array;
  }
}
