package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import java.io.IOException;

public class ConnectionProvider {

  private final Settings settings;
  private static final boolean transactional = Boolean.parseBoolean(Configuration.getProperty(CSD_PROPERTY.LEANXCALE_TRANSACTIONAL, "false"));

  public ConnectionProvider(String LX_URL){

    try {
      settings = Settings.parse(LX_URL);
      if (settings.isTransactional() && !transactional) {
        System.out.println("Disabling logging and conflicts checking");
        settings.disableConflictChecking();
        settings.disableLogging();
      }
      System.out.println("Loaded connection settings:"+settings);
    } catch (LeanxcaleException ex) {
      throw new RuntimeException(ex);
    }
    if (0 == settings.getLastSessionCloseTimeout()) {
      settings.deferSessionFactoryClose(1000);
    }
  }

  public Session getNewSession() throws IOException, LeanxcaleException {
    return SessionFactory.newSession(settings);
  }

}
