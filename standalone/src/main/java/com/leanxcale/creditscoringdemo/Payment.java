package com.leanxcale.creditscoringdemo;

public class Payment {
  private long id;
  private long month;
  private long ts;
  private double installment;
  private double paid;

  public Payment() { }

  public Payment(long id, long month, long ts, double installment, double paid) {
    this.id = id;
    this.month = month;
    this.ts = ts;
    this.installment = installment;
    this.paid = paid;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getMonth() {
    return month;
  }

  public void setMonth(long month) {
    this.month = month;
  }

  public double getInstallment() {
    return installment;
  }

  public void setInstallment(double installment) {
    this.installment = installment;
  }

  public double getPaid() {
    return paid;
  }

  public void setPaid(double paid) {
    this.paid = paid;
  }

  public long getTs() {
    return ts;
  }

  public void setTs(long ts) {
    this.ts = ts;
  }

  @Override
  public String toString() {
    return "Payment{" + "id=" + id + ", month=" + month + ", ts=" + ts + ", installment=" + installment + ", paid=" +
        paid + '}';
  }
}
