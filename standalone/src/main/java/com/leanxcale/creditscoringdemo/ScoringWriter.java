package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class ScoringWriter extends Writer<Scoring>{
  public ScoringWriter(LinkedBlockingQueue<Scoring> queue, ConnectionProvider connectionProvider) throws IOException, LeanxcaleException {
    super(queue, connectionProvider);
    System.out.println("Starting Scoring Writer thread:"+Thread.currentThread().getId());
  }

  @Override
  protected String getTableName() {
    return "SCORING";
  }

  @Override
  protected Tuple createTuple(Scoring obj) {
    StandaloneProcessor.SCORING_COUNT.incrementAndGet();
    Tuple tuple = table.createTuple();
    tuple.put("ID", obj.getId());
    tuple.put("SCORE", obj.getScore());
    tuple.put("APPROVED", obj.isApproved());
    return tuple;
  }

  @Override
  protected int getWriteBatchSize() {
    return Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_WRITER_SCORING_BATCH, "10000"));
  }
}
