package com.leanxcale.creditscoringdemo;

import com.leanxcale.creditscoringdemo.Configuration.CSD_PROPERTY;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.tuple.Tuple;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class PaymentsWriter extends Writer<Payment>{

  public PaymentsWriter(LinkedBlockingQueue<Payment> queue, ConnectionProvider connectionProvider) throws IOException, LeanxcaleException {
    super(queue, connectionProvider);
    System.out.println("Starting Payments Writer thread:"+Thread.currentThread().getId());
  }

  @Override
  protected String getTableName() {
    return "PAYMENTS";
  }

  @Override
  protected Tuple createTuple(Payment obj) {
    StandaloneProcessor.PAYMENTS_COUNT.incrementAndGet();
    Tuple tuple = table.createTuple();
    tuple.put("ID", obj.getId());
    tuple.put("MONTH", obj.getMonth());
    tuple.put("TS", obj.getTs());
    tuple.put("INSTALLMENT", obj.getInstallment());
    tuple.put("PAID", obj.getPaid());
    return tuple;
  }

  @Override
  protected int getWriteBatchSize() {
    return Integer.parseInt(Configuration.getProperty(CSD_PROPERTY.PROCESSOR_WRITER_PAYMENT_BATCH, "10000"));
  }
}
