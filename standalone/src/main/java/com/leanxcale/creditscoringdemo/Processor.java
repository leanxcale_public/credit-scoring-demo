package com.leanxcale.creditscoringdemo;

import com.leanxcale.kivi.tuple.Tuple;
import org.apache.commons.lang.ArrayUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;

/** Pulls the info from the previous queue, process it, and store the results in output queues */
public class Processor extends Thread{
  private static final double LOAN_APPROVAL_CUT_OFF = 425;

  private final Calendar paymentsCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final Calendar monthsFromCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final Calendar monthsToCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private final DateFormat formater = new SimpleDateFormat("MMM-yyyy", Locale.US);

  private final LinkedBlockingQueue<Tuple> inputQueue;
  private final LinkedBlockingQueue<Scoring> scoringQueue;
  private final LinkedBlockingQueue<Payment> paymentsQueue;

  public Processor(LinkedBlockingQueue<Tuple> input_queue, LinkedBlockingQueue<Scoring> scoring_queue,
      LinkedBlockingQueue<Payment> payments_queue) {
    System.out.println("Starting Processor thread:"+Thread.currentThread().getId());
    this.inputQueue = input_queue;
    this.scoringQueue = scoring_queue;
    this.paymentsQueue = payments_queue;
  }

  @Override
  public void run() {
    while (true){
      try{
        Tuple tuple = inputQueue.take();

        //calculates and store in a queue the loan scoring
        Scoring scoring = processScoring(tuple);
        scoringQueue.put(scoring);

        if ( ArrayUtils.contains(new int[]{1,2,3}, tuple.get("loan_status", Integer.class))){
          //if loan is accepted generates all monthly payments and store them in another queue
          List<Payment> payments = processPayments(tuple);
          payments.forEach(p-> {
            try {
              paymentsQueue.put(p);
            } catch (InterruptedException e) {}
          });
        }
        StandaloneProcessor.PROCESSOR_COUNT.incrementAndGet();

      }catch (Exception e){
        System.err.println("Error processing data");
        e.printStackTrace();
      }
    }
  }

  /**
   * calculates the loan credit risk scoring according to the algorithm described here:
   *  https://towardsdatascience.com/how-to-develop-a-credit-risk-model-and-scorecard-91335fc01f03
   *
   *  Code and more details here: https://github.com/finlytics-hub/credit_risk_model/blob/master/Credit_Risk_Model_and_Credit_Scorecard.ipynb
   */
  private Scoring processScoring(Tuple tuple) {
    double score = 598;
    long id = tuple.get("id", Long.class);

    score += getGradeScore(tuple, id);
    score += getHomeOwnershipScore(tuple);
    score += getVerificationStatusScore(tuple, id);
    score += getPurposeScore(tuple, id);
    score += getTermScore(tuple, id);
    score += getIntRateScore(tuple);
    score += getAnualIncScore(tuple);
    score += getDtiScore(tuple);
    score += getInqLast6Score(tuple);
    score += getRevolUtiScore(tuple);
    score += getOutPrncpScore(tuple);
    score += getTotalPymntScore(tuple);
    score += getTotalRecIntScore(tuple);
    score += getTotalRevHiLimScore(tuple);
    score += getMonthsSinceEarliestCrLineScore(tuple);
    score += getMonthsSinceIssueDScore(tuple);
    score += getMonthsSinceLastCreditPullD(tuple);

    boolean approved = (score > LOAN_APPROVAL_CUT_OFF);
    return new Scoring(id, score, approved);
  }

  private List<Payment> processPayments(Tuple tuple) {
    long id = tuple.get("id", Long.class);
    int loanPaid = tuple.get("loan_status", Integer.class) == 1 ? 1 : 0;
    double installment = tuple.get("installment", Double.class);
    long issue_d = tuple.get("issue_d", Long.class);
    int term = tuple.get("term", Integer.class);

    List<Payment> payments = new ArrayList<>();
    paymentsCalendar.setTimeInMillis(issue_d);
    if (loanPaid == 1){ //credit paid successfully. Generated a payment with the installment amount for each month
      for (int i = 1; i <= term; i++){
        payments.add(new Payment(id, i, paymentsCalendar.getTimeInMillis(), installment, installment));
        paymentsCalendar.add(Calendar.MONTH, 1);
      }

    }else { //accepted and not paid. Get the total payment ammount and the last payment date and generates a monthly
      //payment with the proportional ammount payed for each month. Then generates a payment 0 for all months from the
      //last paymnent month to the loan's end month.
      String last_pymnt_d = tuple.get("last_pymnt_d", String.class);
      double total_pymnt = tuple.get("total_pymnt", Double.class);
      int monthsPaid = getMonthsBetween(issue_d, last_pymnt_d);
      double monthlyPaid = total_pymnt / monthsPaid;
      for (int i = 1; i <= term; i++) {
        double paid = (i <= monthsPaid) ? monthlyPaid : 0;
        payments.add(new Payment(id, i, paymentsCalendar.getTimeInMillis(), installment, paid));
        paymentsCalendar.add(Calendar.MONTH, 1);
      }
    }
    return payments;
  }

  private  double getMonthsSinceLastCreditPullD(Tuple tuple) {
    int value = tuple.get("mths_since_last_credit_pull_d", Integer.class);
    if (value == 0)
      return 15;
    if (value < 56)
      return 16;
    if ( value >= 56 && value < 61)
      return 30;
    if (value >= 61 && value < 75)
      return 7;
    return 0;
  }

  private  double getMonthsSinceIssueDScore(Tuple tuple) {
    int value = tuple.get("mths_since_issue_d", Integer.class);
    if (value < 79)
      return -9;
    if ( value >= 79 && value < 89)
      return -8;
    if (value >= 89 && value < 100)
      return -8;
    if (value >= 100 && value < 122)
      return -3;
    return 0;
  }

  private  double getMonthsSinceEarliestCrLineScore(Tuple tuple) {
    int value = tuple.get("mths_since_earliest_cr_line", Integer.class);
    if (value == 0)
      return 3;
    if (value < 125)
      return -4;
    if ( value >= 167 && value < 249)
      return 0;
    if (value >= 331 && value < 434)
      return 1;
    return 0;
  }

  private  double getTotalRevHiLimScore(Tuple tuple) {
    double value = tuple.get("total_rev_hi_lim", Double.class);
    if (value == 0)
      return 5;
    if (value < 6381)
      return 9;
    if ( value >= 6381 && value < 19144)
      return 8;
    if (value >= 19144 && value < 25525)
      return 5;
    if (value >= 25525 && value < 35097)
      return 2;
    if (value >= 35097 && value < 54241)
      return 1;
    if (value >= 54241 && value < 79780)
      return 0;
    return 0;
  }

  private  double getTotalRecIntScore(Tuple tuple) {
    double value = tuple.get("total_rec_int", Double.class);
    if (value < 1089)
      return 109;
    if ( value >= 1089 && value < 2541)
      return 94;
    if (value >= 2541 && value < 4719)
      return 66;
    if (value >= 4719 && value < 7260)
      return 34;
    return 0;
  }

  private  double getTotalPymntScore(Tuple tuple) {
    double value = tuple.get("total_pymnt", Double.class);
    if (value < 10000)
      return -155;
    if ( value >= 10000 && value < 15000)
      return -101;
    if (value >= 15000 && value < 20000)
      return -73;
    if (value >= 20000 && value < 25000)
      return -47;
    return 0;
  }

  private  double getOutPrncpScore(Tuple tuple) {
    double value = tuple.get("out_prncp", Double.class);
    if (value < 1286)
      return -88;
    if ( value >= 1286 && value < 6432)
      return -27;
    if (value >= 6432 && value < 9005)
      return -16;
    if (value >= 9005 && value < 10291)
      return -10;
    if (value >= 10291 && value < 15437)
      return -7;
    return 0;
  }

  private  double getRevolUtiScore(Tuple tuple) {
    double value = tuple.get("revol_util", Double.class);
    if (value == 0) //missing
      return -2;
    if (value < 0.1)
      return -4;
    if ( value >= 0.1 && value < 0.2)
      return -14;
    if (value >= 0.2 && value < 0.3)
      return 11;
    if (value >= 0.3 && value < 0.4)
      return -3;
    if (value >= 0.4 && value < 0.5)
      return 8;
    if (value >= 0.5 && value < 0.6)
      return 10;
    if ( value >= 0.6 && value < 0.7)
      return -6;
    if (value >= 0.7 && value < 0.8)
      return 1;
    if (value >= 0.8 && value < 0.9)
      return -1;
    if (value >= 0.9 && value < 1.0)
      return 15;
    return 0; //> 1
  }

  private  double getInqLast6Score(Tuple tuple) {
    double value = tuple.get("inq_last_6mths", Double.class);
    if (value == 0) //missing
      return 3; //we cannot distinguish between 0 (10) and missing(3)
    if ( value == 1 || value == 2)
      return 9;
    if ( value == 3 || value == 4)
      return 6;
    return 0; //> 4
  }

  private  double getDtiScore(Tuple tuple) {
    double value = tuple.get("dti", Double.class);
    if (value < 1.6)
      return 13;
    if ( value >= 1.6 && value < 5.599)
      return 12;
    if (value >= 5.599 && value < 10.397)
      return 8;
    if (value >= 10.397 && value < 15.196)
      return 6;
    if (value >= 15.196 && value < 19.195)
      return 3;
    if (value >= 19.195 && value < 24.794)
      return 1;
    if (value >= 24.794 && value < 35.191)
      return 1;
    return 0; //>35.191
  }

  private  double getAnualIncScore(Tuple tuple) {
    double value = tuple.get("annual_inc", Double.class);
    if (value == 0) //missing
      return 15;
    if (value < 28555)
      return 12;
    if ( value >= 28555 && value < 37440)
      return 11;
    if (value >= 37440 && value < 61137)
      return 10;
    if (value >= 61137 && value < 81872)
      return 9;
    if (value >= 81872 && value < 102606)
      return 8;
    if (value >= 102606 && value < 120379)
      return 5;
    if (value >= 120379 && value < 150000)
      return 4;
    return 0; //> 150k
  }

  private  double getIntRateScore(Tuple tuple) {
    double value = tuple.get("int_rate", Double.class);
    if (value < 7.071)
      return 24;
    if ( value >= 7.071 && value < 10.374)
      return 7;
    if (value >= 10.374 && value <13.676)
      return 1;
    if (value >= 13.676 && value <15.74)
      return 1;
    if (value >= 15.74 && value < 20.281)
      return 0;
    return 0; //>= 20.281
  }

  private  double getTermScore(Tuple tuple, long id) {
    int value = tuple.get("term", Integer.class);
    switch (value) {
      case 36:
        return -2;
      case 60:
        return 0;
      default:
        System.err.println("Invalid term value:["+ value+ "] at id:"+id);
        return 0;
    }
  }

  private  double getPurposeScore(Tuple tuple, long id) {
    String value = tuple.get("purpose", String.class);
    switch (value) {
      case "debt_consolidation":
        return -8;
      case "credit_card":
        return -6;

      case "vacation":
      case "house":
      case "wedding":
      case "medical":
      case "other":
        return 0;

      case "major_purchase":
      case "car":
      case "home_improvement":
        return 0;

      case "educational":
      case "renewable_energy":
      case "small_business":
      case "moving":
        return -12;

      default:
        System.err.println("Invalid purpose value:["+ value+ "] at id:"+id);
        return 0;
    }
  }

  private  double getVerificationStatusScore(Tuple tuple, long id) {
    String value = tuple.get("verification_status", String.class);
    switch (value) {
      case "Source Verified":
        return -7;
      case "Verified":
        return -11;
      case "Not Verified":
        return 0;
      default:
        System.err.println("Invalid verification_status value:["+ value+ "] at id:"+id);
        return 0;
    }
  }

  private  double getHomeOwnershipScore(Tuple tuple) {
    String value = tuple.get("home_ownership", String.class);
    switch (value) {
      case "OWN":
        return -1;
      case "MORTGAGE":
        return 0;
      default:
        return -3;
    }
  }

  private  double getGradeScore(Tuple tuple, long id) {
    String grade = tuple.get("grade", String.class);
    switch (grade){
      case "A":
        return 24;
      case "B":
        return 20;
      case "C":
        return 15;
      case "D":
        return 12;
      case "E":
        return 8;
      case "F":
        return 5;
      case "G":
        return 0;
      default:
        System.err.println("Invalid grade value:["+ grade + "] at id:"+id);
        return 0;
    }
  }

  private int getMonthsBetween(long issue_d, String last_pymnt_d) {
    try {
      if(last_pymnt_d == null || last_pymnt_d.equals("")){
        return 0;
      }
      monthsFromCalendar.setTimeInMillis(issue_d);
      monthsToCalendar.setTime(formater.parse(last_pymnt_d));

      int nMonthFrom = 12 * monthsFromCalendar.get(Calendar.YEAR) + monthsFromCalendar.get(Calendar.MONTH);
      int nMonthTo = 12 * monthsToCalendar.get(Calendar.YEAR) + monthsToCalendar.get(Calendar.MONTH);
      int months = nMonthTo - nMonthFrom;
      return months;
    }catch (ParseException e){
      System.err.println("Cannot parse date:"+last_pymnt_d);
      e.printStackTrace();
      return 0;
    }
  }
}
