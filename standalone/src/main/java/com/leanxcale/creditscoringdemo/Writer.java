package com.leanxcale.creditscoringdemo;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/** Read info from the output processor queues and store it in Leanxcale*/
public abstract class Writer<T> extends Thread{
  private final ConnectionProvider connProvider;
  private int WRITE_BATCH = getWriteBatchSize();

  private LinkedBlockingQueue<T> queue;


  protected abstract String getTableName();
  protected abstract Tuple createTuple(T obj);
  protected abstract int getWriteBatchSize();

  private Session session;
  protected Table table;

  private boolean pendingCommit;
  private long lastCommitTs = 0;

  public Writer(LinkedBlockingQueue<T> queue, ConnectionProvider connProvider) throws IOException, LeanxcaleException {
    this.connProvider = connProvider;
    this.queue = queue;

    session = connProvider.getNewSession();
    table = session.database().getTable(getTableName());
  }

  @Override
  public void run() {
    int cont = 0;
    while (true){
      try{
        T obj = queue.poll();
        if (obj == null){
          long tc = System.currentTimeMillis();
          if (pendingCommit && ( (tc -lastCommitTs ) > 10000) ){
            lastCommitTs = tc;
            System.out.println("[" + System.currentTimeMillis() + "] " + Thread.currentThread().getId()+ "[LX] - " +
                "Committing pending data");
            session.commit();
            pendingCommit = false;
          }
          Thread.sleep(100);
        }else {
          Tuple tuple = createTuple(obj);
          table.upsert(tuple);
          pendingCommit = true;
          cont++;
          if (cont >= WRITE_BATCH) {
            pendingCommit = false;
            lastCommitTs = System.currentTimeMillis();
            session.commit();
            cont = 0;
          }
        }

      }catch (Exception e){
        System.err.println("Error writing data at table:"+getTableName());
        e.printStackTrace();
      }
    }
  }

}
