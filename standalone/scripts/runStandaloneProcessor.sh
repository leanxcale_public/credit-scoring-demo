#!/bin/bash

#This script will be copied from amiUpdater when building AMIs
nohup java -classpath standalone-VERSION-jar-with-dependencies.jar -DCSD_CONFIG=config.properties -Xms8G -Xmx8G com.leanxcale.creditscoringdemo.StandaloneProcessor > nohup_standalone.out 2>&1 &

exit 0
